import 'dart:io';

import 'package:flutter/material.dart';
import 'package:share_into/io/files.dart';

class LibraryPage extends StatefulWidget {
  const LibraryPage({super.key});

  @override
  LibraryPageState createState() => LibraryPageState();
}

class LibraryPageState extends State<LibraryPage> {
  List<String> paths = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: listPaths(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            paths = snapshot.data!;
          }
          return ListView(
            children: paths.map((e) => Text(e)).toList(),
          );
        });
  }

  listPaths() async {
    Directory root = await Local.filesDirectory;
    paths = listPath(root.path);
  }

  List<String> listPath(String path) {
    Directory root = Directory(path);
    return root.listSync(recursive: false).map((e) => e.path).toList();
  }
}

class PathInfo {
  bool directory = false;
  List<PathInfo>? children;
}
