import 'dart:io' as io;

import 'package:drift/drift.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_into/io/cache.dart';

class MissingContentException implements Exception {}

class MissingFilePathException implements Exception {}

class Local {
  int? id;
  String? content;
  String? filePath;

  Local({this.id, this.content, this.filePath});

  static Future<io.Directory> get appDirectory async {
    return await getApplicationSupportDirectory();
  }

  static Future<io.Directory> get databaseDirectory async {
    io.Directory support = await appDirectory;
    return io.Directory(join(support.path, 'database'));
  }

  static Future<io.Directory> get filesDirectory async {
    io.Directory docs = await getApplicationDocumentsDirectory();
    return io.Directory(join(docs.path, 'files'));
  }

  static Future<io.Directory> get unsortedDirectory async {
    io.Directory files = await filesDirectory;
    return io.Directory(join(files.path, 'unsorted'));
  }

  String? get name => basename(filePath!);

  save() async {
    if (content!.isEmpty) {
      throw MissingContentException();
    }
    if (filePath!.isEmpty) {
      throw MissingFilePathException();
    }
    final file = io.File(filePath!);
    if (!await file.exists()) {
      await file.create(recursive: true);
    }
    String buff = await file.readAsString();
    final newContent = '$buff\n${content!}';
    file.writeAsString(newContent);
    index(newContent);
  }

  index(String newContent) async {
    final cache = CacheDatabase();
    final res = cache.select(cache.files)
      ..where((t) => t.name.equals(filePath!));
    if (await res.getSingleOrNull() != null) {
      cache.update(cache.files)
        ..where((t) => t.id.equals(id!))
        ..write(FilesCompanion(
            name: Value(name!),
            content: Value(newContent),
            path: Value(filePath!)));
      return;
    }
    cache.into(cache.files).insert(FilesCompanion(
        name: Value(name!),
        content: Value(newContent),
        path: Value(filePath!)));
  }
}
