import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DataProvider {
  DataProvider();

  static Database? db;
  static Map<DataEvent, List<Function(Database)>> listeners = {};

  static on(DataEvent event, Function(Database) listener) {
    if (listeners[event] == null) {
      listeners[event] = [];
    }
    listeners[event]!.add(listener);
  }

  static Future<Database> open() async {
    if (db != null) {
      return db!;
    }
    String dbpath = await getDatabasesPath();
    dbpath = join(dbpath, 'share_into.db');
    try {
      await Directory(dbpath).create(recursive: true);
    } catch (_) {}
    db = await openDatabase(dbpath);
    for (var listener in listeners[DataEvent.onOpen]!) {
      listener(db!);
    }
    migrate();
    return db!;
  }

  static migrate() {
    for (var listener in listeners[DataEvent.onMigrate]!) {
      listener(db!);
    }
  }
}

enum DataEvent { onOpen, onMigrate }
