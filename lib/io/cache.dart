import 'dart:io' as io;

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path/path.dart' as p;
import 'package:share_into/io/files.dart';

part 'cache.g.dart';

class Files extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  TextColumn get content => text()();
  TextColumn get path => text()();
}

class Tags extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
}

class FileTags extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get fileId => integer()();
  IntColumn get tagId => integer()();
}

@DriftDatabase(tables: [Files, Tags, FileTags])
class CacheDatabase extends _$CacheDatabase {
  CacheDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;
}

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final filePath = p.join(Local.databaseDirectory.toString(), 'cache.sqlite');
    final file = io.File(filePath);
    return NativeDatabase.createInBackground(file);
  });
}
